#! /bin/bash

# TODO Present argument options and man page?

DOWN_TIME=30  # constant
announced=0
last_up=1
start_time=$(date +%s)

notify_down () {
        /usr/bin/notify-send -t 1000 \
                "Connection seems to be down..."
}
notify_up () {
        /usr/bin/notify-send -t 1000 \
                "Connection seems to be back up..."
}

while true; do
        /bin/ping -c 1 google.ca &> /dev/null

        if [ $? != 0 ]; then
                time_now=$(date +%s)
                if [ $announced != 1 ] && [ $(($time_now - $start_time)) -ge \
                        $DOWN_TIME ]; then
                        announced=1
                        notify_down
                fi


                last_up=0

        else 
                if [ $last_up != 1 ] && [ $(($time_now - $start_time)) -ge \
                        $DOWN_TIME ]; then
                        notify_up
                fi

                announced=0
                last_up=1
                start_time=$(date +%s)
        fi
done
